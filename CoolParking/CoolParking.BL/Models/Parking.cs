﻿using CoolParking.BL.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking parking;
        public List<Vehicle> Vehicles { get; set; } 
        public decimal Balance { get; set; }  

        public decimal CurrentIncome { get; set; }

        private Parking()
        {
            Vehicles = new List<Vehicle>();
            /*Vehicles.Add(new Vehicle("AA-1234-BB", (VehicleType)1, 1000000M));
            Vehicles.Add(new Vehicle("NA-4321-DA", (VehicleType)2, 10000M));
            Vehicles.Add(new Vehicle("QS-2164-CC", (VehicleType)3, 100M));
            Vehicles.Add(new Vehicle("QS-2164-CQ", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-CW", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-CE", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-CR", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-CT", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-CY", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-CU", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-CI", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-QQ", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-WQ", (VehicleType)3, 1M));
            Vehicles.Add(new Vehicle("QS-2164-EQ", (VehicleType)3, 1M));*/
            Balance = Settings.StartParkingBalance;
        }

        public static Parking getParking()
        {
            if (parking == null)
                parking = new Parking();
            return parking;
        }

        public void CleanParking()
        {
            Vehicles = new List<Vehicle>();
        }

    }
}