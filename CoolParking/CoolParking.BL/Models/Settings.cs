﻿
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartParkingBalance = 0;
        public static int ParkingCapacity = 10;

        public static int PaymentTimer = 5000;

        public static int LoggingTimer  = 60000;

        public static Dictionary<VehicleType, decimal> VehicleCost { get; set; } = new Dictionary<VehicleType, decimal>()
        {
            [VehicleType.PassengerCar] = 2M,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Truck] = 5M,
            [VehicleType.Motorcycle] = 1M,
        };

        public static decimal PenaltyCoefficient = 2.5M;

        public static string LogFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
    }
}