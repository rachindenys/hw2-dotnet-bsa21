﻿
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string ActionMessage { get; }
        public decimal Sum { get; }
        public DateTime DateTime { get; }
        public string VehicleId { get; }

        public TransactionInfo(string vehicleId, decimal sum)
        {
            VehicleId = vehicleId;
            Sum = sum;
            DateTime = DateTime.Now;
            ActionMessage = $"[{DateTime}]: Created new transaction for vehicle {vehicleId}. Sum of transaction: {sum}\n";
        }

    }
}