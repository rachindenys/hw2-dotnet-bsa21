﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get;}
        public VehicleType VehicleType { get; }

        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            var r = new Regex($"[A-z][A-z]-[0-9][0-9][0-9][0-9]-[A-z][A-z]");
            if (r.IsMatch(id) && balance > 0)
            {
                Id = id.ToUpper();
            }
            else
            {
                throw new ArgumentException("Bad vehicle ID!");
            }
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string id = "";
            Random rnd = new Random();
            for (int i=0; i < 4; i++)
            {
                id += (char)rnd.Next('a', 'z');
                if (id.Length == 2)
                {
                    id += '-';
                    for(int j=0; j < 4; j++)
                    {
                        id += (int)rnd.Next(0, 9);
                    }
                    id += '-';
                }
            }
            return id;
        }
    }
}