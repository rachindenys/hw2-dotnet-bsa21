﻿
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logPath = Settings.LogFilePath;
        public string LogPath
        {
            get
            {
                return logPath;
            }
            set
            {
                logPath = value;
            }
        }

        public LogService(string logPath) => LogPath = logPath;



        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("File not found!");
            }
            using(StreamReader file = new StreamReader(LogPath))
            {
                return file.ReadToEnd();
            }
        }

        public async void Write(string logInfo)
        {
            using (StreamWriter file = new StreamWriter(path : LogPath, append: true))
            {
                await file.WriteLineAsync(logInfo);
                file.Flush();
            }
        }
    }
}