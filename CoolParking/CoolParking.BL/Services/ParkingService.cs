﻿
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;
using System.Linq;
using System;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private List<TransactionInfo> _transactions;


        ILogService LogService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.getParking();
            _parking.CurrentIncome = 0M;
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Interval = Settings.PaymentTimer;
            _withdrawTimer.Elapsed += AddNewParkingTransaction;
            _withdrawTimer.Start();
            _logTimer = logTimer;
            _logTimer.Interval = Settings.LoggingTimer;
            _logTimer.Elapsed += LogNewParkingTransactions;
            _logTimer.Start();
            LogService = logService;
            _transactions = new List<TransactionInfo>();
            
        }

        private void AddNewParkingTransaction(object source, ElapsedEventArgs e)
        {
            foreach (var v in _parking.Vehicles)
            {
                decimal price = Settings.VehicleCost[v.VehicleType];
                if (v.Balance < price)
                {
                    price *= (decimal)Settings.PenaltyCoefficient;
                }
                v.Balance -= price;
                _parking.Balance += price;
                var tr = new TransactionInfo(v.Id, price);
                _transactions.Add(tr);
                _parking.CurrentIncome += price;
            }
        }

        private void LogNewParkingTransactions(object source, ElapsedEventArgs e)
        {
            var logInfo = "";
            foreach (var tr in _transactions)
            {
                logInfo += tr.ActionMessage;
            }
            _transactions.Clear();
            _parking.CurrentIncome = 0M;
            LogService.Write(logInfo);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count() == Settings.ParkingCapacity)
            {
                throw new InvalidOperationException("Parking is full!");
            }
                if (_parking.Vehicles.Count(x => x.Id == vehicle.Id) > 0)
                {
                    throw new ArgumentException("Bad ID!");
                }
            _parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.CleanParking();
            _parking.Balance = Settings.StartParkingBalance;
        }

        public decimal GetBalance() => _parking.Balance;

        public decimal GetCurrentIncome() => _parking.CurrentIncome;

        public int GetCapacity() => Settings.ParkingCapacity;

        public int GetFreePlaces() => Settings.ParkingCapacity - _parking.Vehicles.Count;



        public TransactionInfo[] GetLastParkingTransactions() => _transactions.ToArray();

        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(_parking.Vehicles);

        public string ReadFromLog()
        {
            return LogService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("Bad ID!");
            }
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Vehicle balance is negative!");
            }
            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.Vehicles.Find(vehicle => vehicle.Id == vehicleId);
            if (vehicle == null || sum<0)
            {
                throw new ArgumentException("Bad arguments!");
            }
            vehicle.Balance += sum;
        }
    }
}